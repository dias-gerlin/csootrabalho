﻿using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSSOTrabalho.Controllers
{
    [Route("api/receita")]
    [ApiController]
    public class ReceitaController : Controller
    {
        ReceitaAppServico receitaServico = new ReceitaAppServico();

        [HttpGet]
        public ActionResult<IEnumerable<Receita>> Listar()
        {
            return Ok(this.receitaServico.Listar());
        }

        // GET api/values/5
        [HttpGet]
        [Route("{codigo}")]
        public ActionResult<Receita> Recuperar(int codigo)
        {
            return Ok(this.receitaServico.Recuperar(codigo));
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Receita> Cadastrar([FromBody] ReceitaRequest value)
        {
            var receitaInserida = receitaServico.CadastrarReceita(value);
            return Ok(receitaInserida);
        }

        // PUT api/values/5
        [HttpPut]
        [Route("{codigo}")]
        public ActionResult<Receita> Editar(int codigo, [FromBody] ReceitaRequest valor)
        {
            var response = this.receitaServico.Editar(codigo, valor);
            return Ok(response);
        }

        // DELETE api/values/5
        [HttpDelete]
        [Route("{codigo}")]
        public ActionResult Excluir(int codigo)
        {
            this.receitaServico.Excluir(codigo);
            return Ok();
        }



    }
}
