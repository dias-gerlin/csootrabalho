﻿using System.Collections.Generic;

namespace CSSOTrabalho.Controllers
{
    public class Receita
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public IEnumerable<string> Ingredientes { get; set; }
        public IEnumerable<string> Etapas { get; set; }
        public string NomeChef { get; set; }
        public string DescricaoBreve { get; set; }
        public Receita(string Titulo,  string NomeChef, string DescricaoBreve,IEnumerable<string> Ingredientes, IEnumerable<string> Etapas)
        {
            this.SetTitulo(Titulo);
            this.SetNomeChef(NomeChef);
            this.SetIngredientes(Ingredientes);
            this.SetEtapas(Etapas);
            this.SetDescricaoBreve(DescricaoBreve);
        }

        public string GetDescricaoBreve()
        {
            return this.DescricaoBreve;
        }

        public void SetDescricaoBreve(string value)
        {
            this.DescricaoBreve = value;
        }


        public IEnumerable<string> GetIngredientes()
        {
            return this.Ingredientes;
        }

        public void SetIngredientes(IEnumerable<string> value)
        {
            this.Ingredientes = value;
        }


        public IEnumerable<string> GetEtapas()
        {
            return this.Etapas;
        }

        public void SetEtapas(IEnumerable<string> value)
        {
            this.Etapas = value;
        }


        public string GetNomeChef()
        {
            return this.NomeChef;
        }

        public void SetNomeChef(string value)
        {
            this.NomeChef = value;
        }

        

        public string GetTitulo()
        {
            return this.Titulo;
        }

        public void SetTitulo(string value)
        {
            this.Titulo = value;
        }

        public override string ToString()
        {
            var texto = $"{this.Titulo};{this.NomeChef};{DescricaoBreve};";
            texto += "[";
            foreach (var item in this.Etapas)
            {
                texto += item+",";
            }
            texto += "];";
            texto += "[";
            foreach (var item in this.Ingredientes)
            {
                texto += item + ",";
            }
            texto += "];";
            return texto;
        }
    }
}