﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CSSOTrabalho.Controllers
{
    public class ReceitaAppServico
    {
        public ReceitaAppServico() { }

        public Receita CadastrarReceita(ReceitaRequest receitaInserir)
        {
            Receita receitaCadastro = new Receita(receitaInserir.Titulo,receitaInserir.NomeChef,receitaInserir.DescricaoBreve, receitaInserir.Ingredientes, receitaInserir.Etapas);
            return this.Cadastrar(receitaCadastro);
        }

        public Receita EditarReceita(int codigo, ReceitaRequest receitaEditar)
        {
            var receita = this.Validar(codigo);

            if(!string.IsNullOrWhiteSpace(receitaEditar.DescricaoBreve))
                receita.SetDescricaoBreve(receitaEditar.DescricaoBreve);

            if (receitaEditar.Etapas.Any())
                receita.SetEtapas(receitaEditar.Etapas);

            if (receitaEditar.Ingredientes.Any())

                receita.SetIngredientes(receitaEditar.Ingredientes);

            if (!string.IsNullOrWhiteSpace(receitaEditar.NomeChef))
                receita.SetNomeChef(receita.NomeChef);

            if (!string.IsNullOrWhiteSpace(receitaEditar.Titulo))
                receita.SetTitulo(receita.Titulo);
            return receita;
        }

        public IEnumerable<Receita> ListarReceita()
        {
            //query = this.repositorioReceita.query();
            return null;
        }

        public Receita Validar(int codigo)
        {
            //query = this.repositorioReceita.query();
            //query = query.Where(x => x.Codigo == codigo)

            return null;
        }


        public Receita Cadastrar(Receita receitaCadastro)
        {
            int count= 0;
            try
            {
                var banco = new FileStream("./BancodeDadoos.txt", FileMode.OpenOrCreate);
                using (var escritor = new StreamWriter(banco))
                using (var leitor = new StreamReader(banco))
                { 
                    var texto = leitor.ReadToEnd();
                    var valores = texto.Split("\n");
                    count = valores.Count() - 1;
                    escritor.WriteLine(count+";" + receitaCadastro);
                    escritor.Flush();
                    leitor.Close();
                }

            }
            catch { }
            receitaCadastro.Id = count;
            return receitaCadastro;
        }
        public Receita Editar(int codigo, ReceitaRequest receita)
        {
            var receitaInserir = new Receita(receita.Titulo, receita.NomeChef, receita.DescricaoBreve, receita.Ingredientes, receita.Etapas);
            int count = 0;
            List<string> registros = new List<string>();
            try
            {
                
                using (var leitor = new StreamReader("./BancodeDadoos.txt"))
                {

                    while(!leitor.EndOfStream)
                    {
                        registros.Add(leitor.ReadLine());
                        count++;
                    }
                    leitor.Dispose();
                    
                }
                count = 0;
                using (var escritor = new StreamWriter("./BancodeDadoos.txt", false))
                {
                    foreach (var item in registros)
                    {
                        if (codigo == count)
                        {
                            escritor.WriteLine(codigo+";"+ receitaInserir);
                        }
                        else
                        {
                            escritor.WriteLine(item);
                        }
                        count++;
                    }
                    escritor.Flush();
                }
                receitaInserir.Id = codigo;
                return receitaInserir;
            }
            catch{ return null; }
        }
        public Receita Recuperar(int codigo)
        {
            string texto = "";
            var banco = new FileStream("./BancodeDadoos.txt", FileMode.Open);
            using (var leitor = new StreamReader(banco))
            {
                texto = leitor.ReadToEnd();
            }
            var valores = texto.Split("\n");

            return Instanciar(valores[codigo]);
        }
        public List<Receita> Listar()
        {
            string texto;
            List<Receita> registros = new List<Receita>();
            var banco = new FileStream("./BancodeDadoos.txt", FileMode.Open);
            using (var leitor = new StreamReader(banco))
            {
                texto = leitor.ReadToEnd();
            }
            var valores = texto.Split("\n");
            foreach (var receita in valores)
            {
                Receita receitaInstanciada = Instanciar(receita);
                if (receitaInstanciada != null){
                    registros.Add(receitaInstanciada);
                }

            }
            return registros;
        }
        public void Excluir(int codigo)
        {
            int count = 0;
            List<string> registros = new List<string>();
            try
            {

                using (var leitor = new StreamReader("./BancodeDadoos.txt"))
                {

                    while (!leitor.EndOfStream)
                    {
                        registros.Add(leitor.ReadLine());
                        count++;
                    }
                    leitor.Dispose();

                }
                count = 0;
                using (var escritor = new StreamWriter("./BancodeDadoos.txt", false))
                {
                    foreach (var item in registros)
                    {
                        if (codigo == count)
                        {

                        }
                        else
                        {
                            escritor.WriteLine(item);
                        }
                        count++;
                    }
                    escritor.Flush();
                }
            }
            catch { }
        }

        public Receita Instanciar(string registro)
        {
            if (!string.IsNullOrWhiteSpace(registro))
            {
                var atributos = registro.Split(";");
                string Titulo = atributos[1];
                string NomeChef = atributos[2];
                string DescricaoBreve = atributos[3];
                IEnumerable<string> Etapas = InstanciarIEnumerable(atributos[4]);
                IEnumerable<string> Ingredientes = InstanciarIEnumerable(atributos[5]);

                Receita receita = new Receita(Titulo, NomeChef, DescricaoBreve, Ingredientes, Etapas);
                receita.Id = int.Parse(atributos[0]);
                return receita;
            }
            return null;
        }
        public IEnumerable<string> InstanciarIEnumerable(string valor)
        {
            valor = valor.Replace('[', ' ');
            valor = valor.Replace(']', ' ');
            valor = valor.Trim();
            List<string> valores = new List<string>();
            foreach (var item in valor.Split(","))
            {
                if(!string.IsNullOrWhiteSpace(item))
                valores.Add(item);
            }
            return valores;
        }
    }
}
