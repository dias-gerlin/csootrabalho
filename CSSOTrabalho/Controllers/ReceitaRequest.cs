﻿using System.Collections.Generic;

namespace CSSOTrabalho.Controllers
{
    public class ReceitaRequest
    {
        public string Titulo { get; set; }
        public string DescricaoBreve { get; set; }
        public IEnumerable<string> Ingredientes { get; set; }
        public IEnumerable<string> Etapas { get; set; }
        public string NomeChef { get; set; }
    }
}